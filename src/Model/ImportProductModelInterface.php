<?php

namespace App\Model;


interface ImportProductModelInterface
{
    /**
     * @return array
     */
    public function toArray(): array;
}
<?php

namespace App\Service;

use App\Form\ImportProductModelFormType;
use App\Model\ImportProductModel;
use App\Validator\Constraint\ImportProductModel as ImportProductModelConstraint;
use League\Csv\Reader;
use Symfony\Component\Form\Forms;

/**
 * Class ImportProductService
 *
 * @package App\Service
 */
class ImportProductService extends AbstractImportService
{
    /**
     * @param Reader $reader
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function import(Reader $reader)
    {
        $products = [];

        foreach ($reader as $record) {
            $productModel = new ImportProductModel();
            $record = array_values($record);

            $productModel->code = $record[0];
            $productModel->name = $record[1];
            $productModel->description = $record[2];
            $productModel->stock = $record[3];
            $productModel->code = $record[4];
            $productModel->discontinued = $record[5];

            $products[] = $productModel;
        }

        $formFactory = Forms::createFormFactory();

        foreach ($products as $product) {
            $form = $formFactory->createBuilder(ImportProductModelFormType::class, $product)->getForm();
            $form->submit($product->toArray());

            if ($form->isValid()) {
                $this->save($product);
                $this->increaseImportedTotalCount();
            } else {
                if (count($form->getErrors())) {
                    $this->addError(sprintf('Product "%s" can not be imported: %s', $product->code, (string)$form->getErrors()));
                }
                $this->increaseNotImportedTotalCount();
            }
        }
    }

    /**
     * @param ImportProductModel $product
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function save(ImportProductModel $product)
    {
        if ($this->env === 'prod') {
            $connection = $this->entityManager->getConnection();
            $connection->executeQuery(
                sprintf("INSERT INTO tblproductdata (`strProductName`, `strProductDesc`, `strProductCode`, `dtmAdded`, `dtmDiscontinued`, `stmTimestamp`, `cost`, `stock`) VALUES (:name, :description, :code, :dtmAdded, :dtmDiscontinued, :stmTimestamp, :cost, :stock)"),
                [
                    ':name' => $product->name,
                    ':description' => $product->description,
                    ':code' => $product->code,
                    ':dtmAdded' => $product->dateAdded,
                    ':dtmDiscontinued' => $product->dtmDiscontinued,
                    ':stmTimestamp' => $product->timestamp,
                    ':cost' => $product->cost,
                    ':stock' => $product->stock,
                ]
            );
        }
    }
}
<?php

namespace App\Command;

use App\Service\ImportProductService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use League\Csv\Reader;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportProductCommand extends Command
{
    protected static $defaultName = 'import:product';
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ImportProductCommand constructor.
     *
     * @param null               $name
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('Import products.')
            ->setHelp('This command import products to database...')
            ->addOption('path',  'p', InputOption::VALUE_REQUIRED, 'The path to CSV file.')
            ->addArgument('prod',  InputArgument::OPTIONAL, 'PROD import', true)
            ->addArgument('dev',  InputArgument::OPTIONAL, 'DEV import', false);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \League\Csv\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getOption('path');

        if (!file_exists($path)) {
            $output->writeln('CSV file was not found!');
            return;
        }

        $fileProduct = Reader::createFromPath($path, 'r');
        $fileProduct->setHeaderOffset(0);

        $importProductService = $this->container->get(ImportProductService::class);
        $importProductService->setEnvironment($input->getArgument('dev'));

        $importProductService->import($fileProduct);

        $errors = $importProductService->getErrors();

        $output->writeln('Importing finished.');
        $output->writeln(sprintf('Count imported: %s', $importProductService->getImportedTotalCount()));
        $output->writeln(sprintf('Count not imported: %s', $importProductService->getNotImportedTotalCount()));
        $output->writeln(sprintf('Count errors: %s', count($errors)));

        if ($errors) {
            $output->writeln('Error list:');
        }

        foreach ($errors as $error) {
            $output->writeln(sprintf('<error>%s</error>', $error));
        }
    }
}
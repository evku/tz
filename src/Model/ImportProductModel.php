<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ImportProductModel
 *
 * @package App\Model
 */
class ImportProductModel implements ImportProductModelInterface
{
    const MAX_COST_PRODUCT = 1000;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     */
    public $stock;

    /**
     * @var float
     */
    public $cost;

    /**
     * @var string
     */
    public $discontinued;

    /**
     * @var string
     */
    public $dtmDiscontinued;

    /**
     * @var \DateTime
     */
    public $dateAdded;

    /**
     * @var string
     */
    public $timestamp;

    /**
     * ImportProductModel constructor.
     */
    public function __construct()
    {
        $this->dateAdded = new \DateTime();
        $this->timestamp = $this->dateAdded->getTimestamp();
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'code'         => $this->code,
            'name'         => $this->name,
            'description'  => $this->description,
            'stock'        => $this->stock,
            'cost'         => $this->cost,
            'discontinued' => $this->discontinued,
        ];
    }

    /**
     * @return array
     */
    public static function getDiscontinuedListPossible(): array
    {
        return [
            'yes' => 'yes',
            'no' => 'no',
        ];
    }
}
<?php

namespace App\Form;


use App\Model\ImportProductModel;
use App\Validator\Constraint\ImportProductModel as ImportProductModelConstraints;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Class ImportProductModelFormType
 *
 * @package App\Form
 */
class ImportProductModelFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'code',
                TextType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add(
                'name',
                TextType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add('description', TextType::class)
            ->add(
                'stock',
                IntegerType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add(
                'cost',
                MoneyType::class,
                [
                    'constraints' => [
                        new NotBlank(),
                        new Range(
                            [
                                'max'        => ImportProductModel::MAX_COST_PRODUCT,
                                'maxMessage' => "You cannot be taller than {{ limit }}$ to import"
                            ]
                        ),
                    ]
                ]
            )
            ->add(
                'discontinued',
                ChoiceType::class,
                [
                    'choices' => ImportProductModel::getDiscontinuedListPossible(),
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            );

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $importProduct = $event->getData();

            if (!$importProduct) {
                return;
            }

            if ('yes' === $importProduct['discontinued']) {
                $importProduct->dtmDiscontinued = new \DateTime();
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'     => ImportProductModel::class,
                'constraints'    => [
                    new ImportProductModelConstraints(),
                ],
                'error_bubbling' => true,
            ]
        );
    }
}
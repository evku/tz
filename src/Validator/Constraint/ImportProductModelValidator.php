<?php

namespace App\Validator\Constraint;

use App\Model\ImportProductModel;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class ImportProductModelValidator
 *
 * @package App\Validator\Constraints
 */
class ImportProductModelValidator extends ConstraintValidator
{
    /**
     * @param ImportProductModel|mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value->cost < 5 && $value->stock < 10) {
            $this->context
                ->buildViolation(
                    sprintf('Product "%s" has cost less then 5$ and less then 10 items in stock', $value->code)
                )
                ->addViolation();
        }

    }
}
<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractImportService
 */
abstract class AbstractImportService
{
    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $env;

    /**
     * @var int
     */
    private $importedTotalCount = 0;

    /**
     * @var int
     */
    private $notImportedTotalCount = 0;

    /**
     * AbstractImportService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $env
     *
     * @return $this
     */
    public function setEnvironment(string $env): AbstractImportService
    {
        $this->env = $env;

        return $this;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return int
     */
    public function getImportedTotalCount(): int
    {
        return $this->importedTotalCount;
    }

    /**
     * @return int
     */
    public function getNotImportedTotalCount(): int
    {
        return $this->notImportedTotalCount;
    }

    /**
     * @return AbstractImportService
     */
    public function increaseImportedTotalCount(): AbstractImportService
    {
        $this->importedTotalCount++;

        return $this;
    }

    /**
     * @return AbstractImportService
     */
    public function increaseNotImportedTotalCount(): AbstractImportService
    {
        $this->notImportedTotalCount++;

        return $this;
    }

    /**
     * @param string $error
     *
     * @return AbstractImportService
     */
    public function addError(string $error): AbstractImportService
    {
        $this->errors[] = $error;

        return $this;
    }

    abstract public function import(Reader $reader);
}